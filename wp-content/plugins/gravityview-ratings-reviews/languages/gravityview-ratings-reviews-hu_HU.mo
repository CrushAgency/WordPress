��    &      L  5   |      P  	   Q  B   [  >   �  	   �     �     �  ?   �  	   =     G     M     S  a   b     �     �     �  
   �                !     -     2     >     \     n     {  $   �  *   �  )   �  :     �   F     �     �  %   �  ]     9   d  )   �     �  [  �     0  i   =  g   �               ,  B   C     �     �  
   �     �  �   �      6     W  #   \     �     �     �     �  	   �     �  "        %     A  $   U  ;   z  .   �  0   �  N     d   e     �     �  '   �  l   �  @   k  .   �  
   �     #                   $         !                           "                                         
                   	   %                      &                                 % Reviews %1$s rating based on %2$s rating %1$s rating based on %2$s ratings %1$s rating based on %2$s vote %1$s rating based on %2$s votes %s rating 1 Review Cancel reply Could not activate the %s Extension; GravityView is not active. Edit This Email Entry Leave a Review Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> More reviews text: Name No Review. Be the First! No Reviews No review text: One review text: Post Review Rate Reply to %s Required fields are marked %s Review this entry Reviews Link Reviews of this entry Show average rating (stars or votes) Text to display when there are no reviews. Text to display when there is one review. The %s Extension requires GravityView Version %s or newer. The placeholder symbol that will be replacedText to display when there are more than one reviews. %s is replaced by the number of reviews. Title Website You have already reviewed this entry. You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s You must be <a href="%s">logged in</a> to post a comment. Your email address will not be published. nounReview Project-Id-Version: GravityView Ratings & Reviews
Report-Msgid-Bugs-To: https://gravityview.co/support/
POT-Creation-Date: 2015-05-08 17:18:53+00:00
PO-Revision-Date: 2015-05-08 17:24+0000
Last-Translator: Zachary Katz <zack@katz.co>
Language-Team: Hungarian (Hungary) (http://www.transifex.com/projects/p/gravityview-ratings-reviews/language/hu_HU/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu_HU
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: grunt-wp-i18n 0.5.2
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
X-Textdomain-Support: yes
 % vélemény Átlagos pontszám: %1$s, %2$s értékelés alapján Átlagos pontszám: %1$s, %2$s értékelés alapján Átlagos értékelés: %1$s, %2$s szavazat alapján Átlagos értékelés: %1$s, %2$s szavazat alapján %s értékelés 1 vélemény Válasz megszakítása Nem lehet aktiválni a %s Bővítményt; a GravityView nem aktív. Szerkesztés E-mail Bejegyzés Értékeld! Bejelentkezve <a href="%1$s">%2$s</a>-ként. <a href="%3$s" title="Kijelentkezés ebből a fiókból">Ki szeretnél jelentkezni?</a> Szöveg ha több vélemény van: Név Nincs vélemény. Legyél az első! Nincs vélemény Szöveg ha nincs vélemény: Szöveg ha egy vélemény van: Vélemény közzététele Pontszám Válasz erre: %s A kötelező mezők jelölése: %s Bejegyzés véleményezése Vélemények linkje Bejegyzéshez érkezett vélemények Átlagos értékelés mutatása (csillagok vagy szavazatok) Megjelenítendő szöveg, ha nincs vélemény. Megjelenítendő szöveg, ha egy vélemény van. A %s bővítménynek szüksége van a GravityView %s vagy újabb verziójára. Megjelenítendő szöveg, ha több vélemény van. %s helyettesítve lesz a vélemények számával. Cím Honlap Már véleményezted ezt a bejegyzést. Ezeket a <abbr title="HyperText Markup Language">HTML</abbr> cimkéket és attribútumokat használhatod: %s <a href="%s">Bejelentkezés</a> szükséges a hozzászóláshoz. Az e-mail címed nem jelenik meg nyilvánosan. Vélemény 