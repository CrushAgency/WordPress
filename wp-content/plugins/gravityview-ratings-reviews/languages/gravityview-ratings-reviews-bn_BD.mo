��          <      \       p   ?   q      �   :   �   I  �   �   <     �  �   �                   Could not activate the %s Extension; GravityView is not active. Entry The %s Extension requires GravityView Version %s or newer. Project-Id-Version: GravityView Ratings & Reviews
Report-Msgid-Bugs-To: https://gravityview.co/support/
POT-Creation-Date: 2015-05-08 17:18:53+00:00
PO-Revision-Date: 2015-05-08 17:24+0000
Last-Translator: Zachary Katz <zack@katz.co>
Language-Team: Bengali (http://www.transifex.com/projects/p/gravityview-ratings-reviews/language/bn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bn
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: grunt-wp-i18n 0.5.2
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
X-Textdomain-Support: yes
 %s এক্সটেনশন সক্রিয় করা যায়নি; গ্রাভিটি ভিউ সক্রিয় নয়। এন্ট্রি %s এক্সটেনশন চালানোর জন্য গ্রাভিটি ভিউ ভার্সন %s অথবা নতুনতর প্রয়োজন। 