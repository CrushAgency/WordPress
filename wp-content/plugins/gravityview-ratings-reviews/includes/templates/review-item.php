<?php
/**
 * The template for displaying review item.
 *
 * @package GravityView_Ratings_Reviews
 * @since 0.1.0
 */
?>

<<?php echo $tag; ?> <?php comment_class( $this->has_children ? 'parent' : '' ); ?> id="review-<?php comment_ID(); ?>" itemprop="review" itemscope itemtype="http://schema.org/Review">

	<div id="gv-div-review-<?php comment_ID(); ?>" class="gv-review-body">

		<div class="gv-review-author">
			<?php
			if ( 0 != $args['avatar_size'] ) {
				if ( 1 === $depth ) {
					$args['avatar_size'] = 80;
				}
				echo get_avatar( $comment, $args['avatar_size'] );
			}
			?>
		</div><!-- .gv-review-author -->

		<div class="gv-review-content<?php echo 1 === $depth ? ' parent' : '' ?>" data-review_id="<?php comment_ID(); ?>">

			<?php if ( '0' == $comment->comment_approved ) : ?>
			<em class="gv-review-awaiting-moderation">
			<?php
			 printf( __( 'This %s is awaiting moderation.', 'gravityview-ratings-reviews' ), 1 === $depth ? 'review' : 'comment' );
			?>
			</em>
			<br />
			<?php endif; ?>

			<?php if ( 1 === $depth ) : ?>
			<div class="gv-review-top-section">
				<?php
				$review_rate  = get_comment_meta( $comment->comment_ID, 'gv_review_rate', true );
				$review_title = get_comment_meta( $comment->comment_ID, 'gv_review_title', true );
				?>

				<?php if ( $review_rate ) : ?>
				<div class="gv-review-rate" itemprop="reviewRating">
					<?php
						if ( 'vote' === $review_rating_type ) {
							GravityView_Ratings_Reviews_Helper::the_vote_rating( array(
								'rating' => $review_rate ? GravityView_Ratings_Reviews_Helper::get_vote_from_star( $review_rate ) : 0,
							) );
						} else {
							GravityView_Ratings_Reviews_Helper::the_star_rating( array(
								'rating' => $review_rate ? $review_rate : 0,
								'type'   => 'rating',
							) );
						}
					?>
				</div><!-- .gv-review-rate -->
				<?php endif; ?>

				<?php if ( ! empty( $review_title ) ) : ?>
				<div class="gv-review-title" itemprop="name">
					<?php echo esc_html( $review_title ); ?>
				</div>
				<?php endif; ?>
			</div><!-- .gv-review-top-section -->
			<?php endif; ?>

			<div class="gv-review-meta">
				<meta itemprop="itemReviewed" content="<?php printf( _x('Entry %s', 'Item being reviewed', 'gravityview-ratings-reviews'), GravityView_Ratings_Reviews_Helper::get_post_bridge_id() ); ?>" />
				<meta itemprop="datePublished" content="<?php echo esc_attr( get_comment_date( 'c', $comment->comment_ID ) ); ?>" />
				<?php
				printf(
					__( '<span class="by">%s</span> <cite class="gv-review-author-link" itemprop="author">%s</cite> on', 'gravityview-ratings-reviews' ),
					__( 'By', 'gravityview-ratings-reviews' ),
					get_comment_author_link()
				);
				?>
				<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
				<?php
					/* translators: 1: date, 2: time */
					printf( __( '%1$s at %2$s', 'gravityview-ratings-reviews' ), get_comment_date(),  get_comment_time() ); ?>
				</a>
				<?php
				GravityView_Ratings_Reviews_Helper::edit_review_link(
					__( '(Edit)', 'gravityview-ratings-reviews' ),
					'&nbsp;&nbsp;',
					'',
					array( 'review_type' => $review_rating_type )
				);
				?>
			</div><!-- .gv-review-meta -->

			<div class="gv-review-content-text" itemprop="reviewBody">
			<?php
			comment_text(
				get_comment_id(),
				array_merge(
					$args,
					array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
					)
				)
			);
			?>
			</div><!-- .gv-review-content-text -->

			<div class="reply">
				<?php
				comment_reply_link(
					array_merge(
						$args,
						array(
							'reply_text' => __( 'Comment on this review', 'gravityview-ratings-reviews' ),
							'login_text' => __( 'Log in to comment', 'gravityview-ratings-reviews' ),
							'add_below'  => $add_below,
							'depth'      => $depth,
							'max_depth'  => $args['max_depth'],
						)
					)
				);
				?>
			</div><!-- .reply -->
		</div><!-- .gv-review-content-->


	</div><!-- .gv-review-body -->

<?php /** $tag is closed by review walker. **/ ?>
