<?php
/**
 * Components loader.
 *
 * @package   GravityView_Ratings_Reviews
 * @license   GPL2+
 * @author    Katz Web Services, Inc.
 * @link      http://gravityview.co
 * @copyright Copyright 2014, Katz Web Services, Inc.
 *
 * @since 0.1.0
 */
class GravityView_Ratings_Reviews_Loader extends GravityView_Extension {

	/**
	 * Name of the plugin, used to fetch updates from GravityView.co
	 *
	 * @see GravityView_Extension::settings()
	 *
	 * @var string
	 */
	protected $_title = 'Ratings & Reviews';

	/**
	 * Minimum version of GravityView required to use the extension
	 *
	 * @see GravityView_Extension::is_extension_supported()
	 *
	 * @var string
	 */
	protected $_min_gravityview_version = '1.7.4';

	/**
	 * Translation textdomain passed to GravityView_Extension to handle loading language files
	 *
	 * @see GravityView_Extension::load_plugin_textdomain()
	 *
	 * @var string
	 */
	protected $_text_domain = 'gravityview-ratings-reviews';

	/**
	 * Components of this extension.
	 *
	 * @since 0.1.0
	 *
	 * @var array
	 */
	protected $components = array(
		'post-bridge',
		'fields',
		'meta-box',
		'meta-box-edit-review',
		'review',
		'form-fields',
		'sorting',
	);

	/**
	 * Component instances.
	 *
	 * @since 0.1.0
	 *
	 * @var array
	 */
	public $component_instances = array();

	/**
	 * Constructor.
	 *
	 * Set properties and load components.
	 *
	 * @since 0.1.0
	 *
	 * @param string $plugin_file
	 * @param string $plugin_version
	 *
	 * @return void
	 */
	public function __construct( $plugin_file, $plugin_version ) {
		$this->plugin_file    = $plugin_file;
		$this->plugin_version = $plugin_version;

		parent::__construct();
	}

	/**
	 * Called by parent's constructor if extension is supported.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public function add_hooks() {
		$this->set_properties();
		$this->load_components();
	}

	/**
	 * Set properties of this extension that will be useful for components.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function set_properties() {
		// Directories.
		$this->dir           = trailingslashit( plugin_dir_path( $this->plugin_file ) );
		$this->includes_dir  = trailingslashit( $this->dir . 'includes' );
		$this->templates_dir = trailingslashit( $this->includes_dir . 'templates' );

		// URLs.
		$this->url     = trailingslashit( plugin_dir_url( $this->plugin_file ) );
		$this->js_url  = trailingslashit( $this->url . 'assets/js' );
		$this->css_url = trailingslashit( $this->url . 'assets/css' );

		// Properties from GravityView_Extension.
		$this->_version     = $this->plugin_version;
		$this->_path = $this->plugin_file;
	}

	/**
	 * Loads components.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function load_components() {
		// Loads the absract component before loading each component.
		require_once $this->includes_dir . 'class-component.php';
		require_once $this->includes_dir . 'class-helper.php';

		// Loads each known components of this extension.
		foreach ( $this->components as $component ) {
			$filename  = $this->includes_dir . 'class-' . $component . '.php';
			$classname = 'GravityView_Ratings_Reviews_' . str_replace( ' ', '_', ucwords( str_replace( '-', ' ', $component ) ) );

			// Loads component and pass extension's instance so that component can
			// talk each other.
			require_once $filename;
			$this->component_instances[ $component ] = new $classname( $this );
			$this->component_instances[ $component ]->load();
		}
	}
}
