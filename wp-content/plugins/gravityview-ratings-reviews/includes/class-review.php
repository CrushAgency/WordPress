<?php
/**
 * Reviews component.
 *
 * Review is a comment.
 *
 * @package   GravityView_Ratings_Reviews
 * @license   GPL2+
 * @author    Katz Web Services, Inc.
 * @link      http://gravityview.co
 * @copyright Copyright 2014, Katz Web Services, Inc.
 *
 * @since 0.1.0
 */
class GravityView_Ratings_Reviews_Review extends GravityView_Ratings_Reviews_Component {

	/**
	 * Field's name that hold View ID value.
	 *
	 * @since 0.1.0
	 *
	 * @var string
	 */
	public $field_view_id = 'gv_view_id';

	/**
	 * Field's name that hold Entry ID value.
	 *
	 * @since 0.1.0
	 *
	 * @var string
	 */
	public $field_entry_id = 'gv_entry_id';

	/**
	 * Field's name that hold post ID containg the view. This might be View ID
	 * or Post ID that has gravityview shortcode being rendered.
	 *
	 * @since 0.1.0
	 *
	 * @var string
	 */
	public $field_post_container_id = 'gv_post_container_id';

	/**
	 * Field's name that hold review's type (e.g., 'vote', 'stars', etc). See
	 * class-meta-box.php for defined review types.
	 *
	 * @since 0.1.0
	 *
	 * @var string
	 */
	public $field_review_type = 'gv_review_type';

	/**
	 * Callback when this component is loaded by the loader.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public function load() {
		// By default `wp_star_rating` is enabled for admin only.
		require_once ABSPATH . 'wp-admin/includes/template.php';

		$this->gravityview_hooks();
		$this->wp_hooks();
	}

	/**
	 * GravityView hooks.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function gravityview_hooks() {

		add_action('gravityview_before', array( $this, 'start_microdata_wrapper') );

		// Render reviews below single entry.
		add_action( 'gravityview_after', array( $this, 'entry_reviews' ), 15 );

		add_action('gravityview_after', array( $this, 'end_microdata_wrapper'), 16 );
	}

	/**
	 * Wrap entry in rich snippets to allow review microdata
	 */
	function start_microdata_wrapper() {
		echo '<div itemscope itemtype="http://schema.org/Thing" id="gv-item-reviewed" itemprop="itemReviewed" itemscope>';
	}

	/**
	 * Wrap entry in rich snippets to allow review microdata
	 */
	function end_microdata_wrapper() {
		echo '</div>';
	}

	/**
	 * WordPress hooks.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function wp_hooks() {
		// Comments.
		add_filter( 'preprocess_comment',           array( $this, 'preprocess_comment' ) );
		add_filter( 'pre_comment_approved',         array( $this, 'pre_comment_approved' ), 10, 2 );
		add_action( 'comment_duplicate_trigger',    array( $this, 'review_duplicate_message' ) );
		add_filter( 'comment_form_logged_in_after', array( $this, 'review_fields' ) );
		add_filter( 'comment_form_after_fields',    array( $this, 'review_fields' ) );
		add_filter( 'comment_post_redirect',        array( $this, 'redirect_to_entry' ), 10, 2 );
		add_action( 'comment_form',                 array( $this, 'inject_fields' ) );
		add_filter( 'comments_open',                array( $this, 'comments_open' ), 10, 2 );
		add_filter( 'comment_class',                array( $this, 'comment_class' ), 10, 4 );
		add_filter( 'get_comment_link',             array( $this, 'get_comment_link' ), 10, 3 );

		add_action( 'comment_trashed_gravityview', array( $this, 'update_counter' ), 15, 2 );
		add_action( 'comment_approved_gravityview', array( $this, 'update_counter' ), 15, 2 );
		add_action( 'wp_insert_comment', array( $this, 'update_counter' ), 15, 2 );
		add_action( 'wp_insert_comment', array( $this, 'update_comment_type' ), 20, 2 );

		add_action( 'gform_delete_lead', array( $this, 'delete_entry_comments' ), 15 );
		add_action( 'gform_update_status', array( $this, 'update_status_entry_comments' ), 15, 3 );

		// Styles and scripts.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * When deleting a Lead/Entry, remove the Bridge post, which will delete the Comments
	 *
	 * @filter gform_delete_lead
	 *
	 * @since 0.1.1
	 *
	 * @uses gravityview_get_entry() Fetch the entry attached to the comment
	 *
	 * @param  int $entry_id Contains id of the entry.
	 *
	 * @return void
	 */
	public function delete_entry_comments( $entry_id ) {
		$entry = gravityview_get_entry( $entry_id );
		$bridge = GravityView_Ratings_Reviews_Helper::get_post_bridge_id( $entry['id'] );
		wp_delete_post( $bridge, true );
	}


	/**
	 * Change the status of the Comments and Bridge Post based on the Lead/Entry
	 *
	 * @filter preprocess_comment
	 *
	 * @since 0.1.1
	 *
	 * @uses gravityview_get_entry() Fetch the entry attached to the comment
	 *
	 * @param  int $entry_id Contains id of the entry.
	 * @param  string $property_value Contains the current status of the lead
	 * @param  string $previous_value Contains the previous status of the lead
	 *
	 * @return void
	 */
	public function update_status_entry_comments( $entry_id, $property_value, $previous_value ) {
		$entry = gravityview_get_entry( $entry_id );
		$bridge = GravityView_Ratings_Reviews_Helper::get_post_bridge_id( $entry['id'] );
		if ( 'trash' === $property_value ){
			$comments = get_comments( array( 'post_id' => $bridge, 'fields' => 'ids', 'status' => '1' ) );
			wp_trash_post( $bridge );
			foreach ( $comments as $id ) {
				wp_set_comment_status( $id, 'trash' );
			}
		} elseif ( 'active' === $property_value ){
			$comments = get_comments( array( 'post_id' => $bridge, 'fields' => 'ids', 'status' => 'post-trashed' ) );
			wp_publish_post( $bridge );
			foreach ( $comments as $id ) {
				wp_set_comment_status( $id, 'approve' );
			}
		}
	}

	/**
	 * Filter a comment's data before it is sanitized and inserted into the database.
	 *
	 * @filter preprocess_comment
	 *
	 * @since 0.1.0
	 *
	 * @uses gravityview_get_entry() Fetch the entry attached to the comment
	 * @param  array $commentdata Contains information on the comment.
	 * @return array Filtered $commentdata
	 */
	public function preprocess_comment( $commentdata ) {

		if ( empty( $_POST[ $this->field_view_id ] ) || empty( $_POST[ $this->field_entry_id ] ) ) {
			return $commentdata;
		}

		//
		// Existing entry might not have post bridge. Or Post ID being passed is
		// the post ID of the post containing gravityview shortcode.
		//
		$post_bridge = $this->loader->component_instances['post-bridge'];

		if ( $post_bridge->name !== get_post_type( $commentdata['comment_post_ID'] ) ) {

			$entry     = gravityview_get_entry( $_POST[ $this->field_entry_id ] );

			$bridge_id = $post_bridge->create_bridge( $entry );

			if ( $bridge_id && ! is_wp_error( $bridge_id ) ) {
				$commentdata['comment_post_ID'] = $bridge_id;
			}
		}

		return $commentdata;
	}

	/**
	 * Filter a comment's approval status before it is set.
	 *
	 * @since 0.1.0
	 *
	 * @param bool|string $approved    The approval status. Accepts 1, 0, or 'spam'.
	 * @param array       $commentdata Comment data.
	 */
	public function pre_comment_approved( $approved, $commentdata ) {
		$post_bridge_comp = $this->loader->component_instances['post-bridge'];

		if ( get_post_type( $commentdata['comment_post_ID'] ) === $post_bridge_comp->name ) {

			$allowed = GravityView_Ratings_Reviews_Helper::is_user_allowed_to_leave_review(
				$commentdata['comment_post_ID'],
				$commentdata['comment_author'],
				$commentdata['comment_author_email']
			);

			if ( ! $allowed ) {
				wp_die( __( 'You have already reviewed this entry.', 'gravityview-ratings-reviews' ) );
			}
		}

		return $approved;
	}

	/**
	 * Updates an existing comment in the database.
	 *
	 * Filters the comment and makes sure certain fields are valid before updating.
	 *
	 * @since 2.0.0
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param array $comment Contains information on the comment.
	 * @return int Comment was updated if value is 1, or was not updated if value is 0.
	 */
	public function update_comment_type( $id, $comment = array() ) {

		// Only process bridge post comments
		if ( ! GravityView_Ratings_Reviews_Helper::is_bridge_post_type( $comment->comment_post_ID ) ) {
			return;
		}

		if ( empty( $comment ) ){
			$comment = get_comment( $id );
		}

		// Only process bridge post comments
		if( ! GravityView_Ratings_Reviews_Helper::is_bridge_post_type( $comment->comment_post_ID ) ) {
			return;
		}

		// Update the comment type to GravityView
		$comment->comment_type = 'gravityview';

		// wp_update_comment expects an array
		$updated = wp_update_comment( (array) $comment );

		if ( empty( $updated ) ) {
			do_action( 'gravityview_log_error', __METHOD__ . ' - Comment did not update.', $comment );
		}

	}

	/**
	 * Set ratings data for each comment
	 *
	 * @since  0.1.1
	 *
	 * @param int $id ID of the comment
	 * @param stdClass $comment Object with comment data (`comment_ID`, `comment_post_ID`, etc)
	 *
	 * @return void
	 */
	public function update_counter( $id = 0, $comment = null ){

		// Only process bridge post comments
		if ( ! GravityView_Ratings_Reviews_Helper::is_bridge_post_type( $comment->comment_post_ID ) ) {
			return;
		}

		// Save the review title
		if ( ! empty( $_POST['gv_review_title'] ) ) {
			$comment_title = $_POST['gv_review_title'];
			if ( current_user_can( 'unfiltered_html' ) ) {
				$comment_title = wp_filter_post_kses( $comment_title );
			} else {
				$comment_title = wp_filter_kses( $comment_title );
			}

			update_comment_meta( $id, 'gv_review_title', $comment_title );
		}

		if ( ! empty( $_POST[ $this->field_review_type ] ) ) {

			switch ( $_POST[ $this->field_review_type ] ) {

				case 'stars':
					$rate = absint( $_POST['gv_review_rate'] );
					break;
				//
				// If review's type is NOT 'stars', we need to convert the value to star
				// based.
				//
				case 'vote':
				default:
					$rate = GravityView_Ratings_Reviews_Helper::get_star_from_vote( $_POST['gv_review_rate'] );
					break;
			}

			// Save the review rating
			update_comment_meta( $id, 'gv_review_rate', $rate );

			// Update the entry meta
			$this->update_entry_meta_counter( $id, $comment );
		}

	}

	/**
	 * Update the entry ratings
	 *
 	 * @since  0.1.1
	 *
	 * @param int $id ID of the comment
	 * @param stdClass $comment Object with comment data (`comment_ID`, `comment_post_ID`, etc)
	 *
	 * @return void
	 */
	function update_entry_meta_counter( $id, $comment ) {
		if ( ! is_object( $comment ) || empty( $comment ) ){
			$ocomment = get_comment( $id );
		}
		$entry_id = get_post_meta( $comment->comment_post_ID, 'gf_entry_id', true );
		$metas = GravityView_Ratings_Reviews_Helper::get_ratings_detailed( $comment->comment_post_ID, $entry_id );

		foreach ( $metas as $meta_key => $meta_value ) {
			gform_update_meta( $entry_id, 'gravityview_ratings_' . $meta_key, $meta_value );
		}
	}

	/**
	 * Change the message when a duplicate review is detected.
	 *
	 * @action comment_duplicate_trigger
	 *
	 * @since 0.1.0
	 *
	 * @param  array $commentdata Contains information on the comment.
	 * @return void
	 */
	public function review_duplicate_message( $commentdata ) {
		$post        = get_post( $commentdata['comment_post_ID'] );
		$post_bridge = $this->loader->component_instances['post-bridge'];

		if ( $post_bridge->name === get_post_type( $post ) ) {
			wp_die( __( 'Duplicate review detected; it looks as though you&#8217;ve already said that!', 'gravityview-ratings-reviews' ) );
		}
	}

	/**
	 * Saves review information as comment meta.
	 *
	 * @action comment_post
	 *
	 * @since 0.1.0
	 *
	 * @param  int $comment_ID The comment ID.
	 * @return void
	 */
	public function save_review_info( $comment_ID ) {


	}

	/**
	 * Filter the default comment form fields to add 'vote' field.
	 *
	 * @since 0.1.0
	 *
	 * @action comment_form_logged_in_after
	 * @action comment_form_after_fields
	 *
	 * @todo This can be removed/moved since we've implemented our own helper
	 *       `GravityView_Ratings_Reviews::review_form`.
	 *
	 * @return void
	 */
	public function review_fields() {
		global $gravityview_view;

		if( ! $this->is_single_context() ||  ! $gravityview_view->getCurrentEntry() ) {
			return;
		}

		$expected_post_types = array(
			'gravityview',
			$this->loader->component_instances['post-bridge']->name,
		);

		if ( ! in_array( get_post_type(), $expected_post_types ) ) {
			return;
		}

		if ( 'vote' === $gravityview_view->getAtts('entry_review_type') ) {
			$rating_field = GravityView_Ratings_Reviews_Helper::get_vote_rating(
				array(
					'rating'    => 0,
					'number'    => 0,
					'clickable' => true,
				)
			);
		} else {
			$rating_field = GravityView_Ratings_Reviews_Helper::get_star_rating(
				array(
					'rating'    => 0,
					'type'      => 'rating',
					'number'    => 0,
					'clickable' => true,
				)
			);
		}

		$fields = array(
			'gv_review_title'  => '<p class="comment-form-gv-review comment-form-gv-review-title"><label for="gv_review_title">' . __( 'Title', 'gravityview-ratings-reviews' ) . '</label> ' . '<input id="gv_review_title" name="gv_review_title" type="text" size="30" /></p>',

			'gv_review_rate' => sprintf(
				'<div class="comment-form-gv-review comment-form-gv-review-rate"><label>%s</label>%s %s</div>',
				__( 'Rate', 'gravityview-ratings-reviews' ),
				$rating_field,
				'<input id="gv_review_rate" class="gv-star-rate-field" name="gv_review_rate" type="hidden" />'
			)
		);

		foreach ( $fields as $name => $field ) {
			echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
		}
	}

	/**
	 * Filter the location URI to send the reviewer back to entry after posting.
	 *
	 * @filter comment_post_redirect
	 *
	 * @since 0.1.0
	 *
	 * @param string $location The 'redirect_to' URI sent via $_POST.
	 * @param object $comment  Comment object.
	 *
	 * @return string Location to entry
	 */
	public function redirect_to_entry( $location, $comment ) {
		return GravityView_Ratings_Reviews_Helper::get_review_permalink( $location, $comment );
	}

	/**
	 * Inject fields required by ratings-reviews extension at the bottom of the
	 * comment form, inside the closing </form> tag.
	 *
	 * @action comment_form
	 *
	 * @since 0.1.0
	 *
	 * @uses  GravityView_API::get_entry_slug() Get the slug for the entry
	 *
	 * @return void
	 */
	public function inject_fields() {
		global $gravityview_view, $gv_view_post_container_id;

		if ( $this->is_single_context() && $entry = $gravityview_view->getCurrentEntry() ) {

			$entry_slug = GravityView_API::get_entry_slug( $entry['id'], $entry );

			printf( '<input type="hidden" name="%s" value="%s">', $this->field_entry_id, $entry_slug );
			printf( '<input type="hidden" name="%s" value="%d">', $this->field_view_id, $gravityview_view->getViewId() );
			printf( '<input type="hidden" name="%s" value="%s">', $this->field_review_type, $gravityview_view->getAtts('entry_review_type') );
			printf( '<input type="hidden" name="%s" value="%d">', $this->field_post_container_id, $gv_view_post_container_id );
		}

	}

	/**
	 * Render entry reviews.
	 *
	 * @todo Add ability to view existing ratings while not adding new ratings
	 *
	 * @filter gv_ratings_reviews_display_reviews Whether to display the reviews
	 * @action gravityview_after
	 *
	 * @since 0.1.0
	 *
	 * @param  int  $view_id
	 * @return void
	 */
	public function entry_reviews( $view_id ) {

		$comments_open = $this->comments_open( false, $view_id );

		/**
		 * Whether to display reviews for the View
		 * @since 1.0.3-beta
		 * @param boolean $display_reviews Whether the commments are open
		 * @param int $view_id ID of the View being displayed
		 */
		$display_reviews = apply_filters( 'gv_ratings_reviews_display_reviews', $comments_open, $view_id );

		if ( $display_reviews ) {

			remove_all_filters( 'comments_open' );

			add_filter( 'comments_open', '__return_true' );

			// Loads review walker.
			require_once $this->loader->includes_dir . 'class-review-walker.php';

			/**
			 * Filter to override gravityview entry's reviews.
			 *
			 * @since 0.1.0
			 *
			 * @param  string $template_path Path to file template for entry's reviews
			 * @return string
			 */
			$template_path = apply_filters(
				'gravityview_entry_comments_template_path',
				$this->loader->templates_dir . 'review-list.php'
			);

			require_once $template_path;

			remove_filter( 'comments_open', '__return_true' );
		}

		unset( $comments_open, $display_reviews, $template_path );
	}

	/**
	 * When a View has allow_entry_reviews enabled then allows reviews/comments
	 * to be rendered.
	 *
	 * @filter comments_open
	 *
	 * @since 0.1.0
	 *
	 * @param bool $is_open Default status
	 * @param int  $post_id
	 *
	 * @return bool
	 */
	public function comments_open( $is_open = false, $post_id ) {
		if ( $this->is_single_context() ) {
			$is_open = $this->is_allowable_to_receive_review( $post_id );
		} else if ( ! empty( $_POST[ $this->field_view_id ] ) ) {
			$is_open = $this->is_allowable_to_receive_review( $_POST[ $this->field_view_id ] );
		}

		return $is_open;
	}

	/**
	 * Adds 'gv-review-item' to review item's class.
	 *
	 * @since 0.1.0
	 *
	 * @param array       $classes    An array of comment classes.
	 * @param string      $class      A comma-separated list of additional classes added to the list.
	 * @param int         $comment_id The comment id.
	 * @param int|WP_Post $post_id    The post ID or WP_Post object.
	 *
	 * @return array An array of classes.
	 */
	public function comment_class( $classes, $class, $comment_id, $post_id ) {
		if ( GravityView_Ratings_Reviews_Helper::get_post_bridge_type() === get_post_type( $post_id ) ) {
			$classes[] = 'gv-review-item';
		}
		return $classes;
	}

	/**
	 * Filter the returned single comment permalink.
	 *
	 * @filter get_comment_link
	 *
	 * @since 0.1.0
	 *
	 * @see get_page_of_comment()
	 *
	 * @param string $link    The comment permalink with '#comment-$id' appended.
	 * @param object $comment The current comment object.
	 * @param array  $args    An array of arguments to override the defaults.
	 */
	public function get_comment_link( $link, $comment, $args ) {
		return GravityView_Ratings_Reviews_Helper::get_review_permalink( $link, $comment );
	}

	/**
	 * Whether a Post is allowable to receive review. This is used by comments_open.
	 *
	 * @since 0.1.0
	 *
	 * @param  int|WP_Post $post The post ID or WP_Post object.
	 * @return bool
	 */
	public function is_allowable_to_receive_review( $post ) {

		$is_allowable = false;

		$post = get_post( $post );
		if ( empty( $post ) || is_wp_error( $post ) ) {
			return $is_allowable;
		}

		if ( $this->is_single_context() || 'gravityview' === get_post_type( $post->ID ) ) {
			$settings     = gravityview_get_template_settings( $post->ID );
			$is_allowable = ! empty( $settings['allow_entry_reviews'] );
		}

		return $is_allowable;
	}

	/**
	 * Is current context is 'single'? Useful when global $gravityview_view object
	 * is instantiated already.
	 *
	 * @since 0.1.0
	 *
	 * @global GravityView_View $gravityview_view
	 *
	 * @return bool
	 */
	public function is_single_context() {
		global $gravityview_view;

		return (
			 ! empty( $gravityview_view )
			 &&
			 'single' === $gravityview_view->getContext()
		);
	}

	/**
	 * Enqueue statics (JS and CSS).
	 *
	 * @action wp_enqueue_scripts
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public function enqueue_scripts() {
		/**
		 * Fires before required scripts and styles are enqueued.
		 *
		 * @since 0.1.0
		 */
		do_action( 'gravityview_ratings_reviews_before_enqueue' );

		$this->register_scripts();
		$this->enqueue_when_needed();

		/**
		 * Fires after required scripts and styles are enqueued.
		 *
		 * @since 0.1.0
		 */
		do_action( 'gravityview_ratings_reviews_after_enqueue' );
	}

	/**
	 * Register public scripts.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function register_scripts() {
		// Allows support of 'dashicons' for WP < 3.8.
		if ( ! wp_style_is( 'dashicons', 'registered' ) ) {
			wp_register_style( 'dashicons', $this->loader->css_url . 'dashicons.min.css' );
		}

		$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '-dev.css' : '.css';
		if ( ! wp_style_is( 'gv-ratings-reviews-public', 'registered' ) ) {
			wp_register_style( 'gv-ratings-reviews-public', $this->loader->css_url . "public{$suffix}", array( 'dashicons' ), $this->loader->plugin_version );
		}

		$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '.js' : '.min.js';
		if ( ! wp_script_is( 'gv-ratings-reviews-public', 'registered' ) ) {
			wp_register_script( 'gv-ratings-reviews-public', $this->loader->js_url . "public{$suffix}", array( 'jquery', 'underscore' ), $this->loader->plugin_version, true );
		}
	}

	/**
	 * Enqueue the public scripts if needed.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	protected function enqueue_when_needed() {
		global $gravityview_view, $post, $wp_scripts;

		$needed = (
			'gravityview' === get_post_type( $post )
			||
			! empty( $gravityview_view	)
			||
			( ! empty( $post->post_content ) && has_shortcode( $post->post_content, 'gravityview' ) )
		);

		// Checking for the Admin pages
		if ( is_admin() && function_exists( 'get_current_screen' ) ){
			$screen = get_current_screen();

			if ( 'comment' === $screen->id && ! empty( $_GET['action'] ) && ! empty( $_GET['c'] ) ){
				$comment = absint( $_GET['c'] );
				$comment = get_comment( $comment );
				$needed = ( 'gravityview' === $comment->comment_type );
			}
		}

		if ( ! $needed ) {
			return;
		}

		wp_enqueue_style( 'gv-ratings-reviews-public' );
		wp_enqueue_script( 'gv-ratings-reviews-public' );

		$exports = sprintf( 'var GV_RATINGS_REVIEWS = %s', json_encode( $this->get_exported_vars() ) );
		$wp_scripts->add_data( 'gv-ratings-reviews-public', 'data', $exports );
	}

	/**
	 * Get exported vars for JS.
	 *
	 * @since 0.1.0
	 *
	 * @return array
	 */
	protected function get_exported_vars() {
		return array(
			'comment_label_when_reply'      => __( 'Comment', 'gravityview-ratings-reviews' ),
			'comment_submit_when_reply'     => __( 'Post Comment', 'gravityview-ratings-reviews' ),
			'comment_to_review_text'        => __( 'Leave comment on this review', 'gravityview-ratings-reviews' ),
			'cancel_comment_to_review_text' => __( 'Cancel comment', 'gravityview-ratings-reviews' ),
			'vote_text_format' => __( '<%= number %> rating', 'gravityview-ratings-reviews' ),
			'vote_up' => GravityView_Ratings_Reviews_Helper::get_vote_rating_text( 1 ),
			'vote_down' => GravityView_Ratings_Reviews_Helper::get_vote_rating_text( -1 ),
			'vote_zero' => GravityView_Ratings_Reviews_Helper::get_vote_rating_text( 0 ),
		);
	}
}
