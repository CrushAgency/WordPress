<?php
/**
 * Component helper that provides helper methods.
 *
 * @package   GravityView_Ratings_Reviews
 * @license   GPL2+
 * @author    Katz Web Services, Inc.
 * @link      http://gravityview.co
 * @copyright Copyright 2014, Katz Web Services, Inc.
 *
 * @since 0.1.0
 */
class GravityView_Ratings_Reviews_Helper {

	/**
	 * Get post type name of the post bridge.
	 *
	 * @since 0.1.0
	 *
	 * @return string
	 */
	public static function get_post_bridge_type() {
		global $gv_ratings_reviews;

		return $gv_ratings_reviews->component_instances['post-bridge']->name;
	}

	/**
	 * Check whether a Post is a bridge post type or not
	 * @param  int|WP_Post  $post_or_post_id  $post object or post ID to check
	 * @return boolean          True: yep, GV bridge. False: nope.
	 */
	public static function is_bridge_post_type( $post_or_post_id ) {

		$comment_post_type = get_post_type( $post_or_post_id );

		$expected_type = self::get_post_bridge_type();

		return $comment_post_type === $expected_type;
	}

	/**
	 * Retrieve reviews. Post being passed should be the post bridge.
	 *
	 * @since 0.1.0
	 *
	 * @param int|WP_Post $post_id Optional. Post ID or WP_Post object. Default is global $post.
	 * @param string $status Status for reviews. Default: approve
	 *
	 * @return array Reviews arrays
	 */
	public static function get_reviews( $post = 0, $status = 'approve' ) {
		global $gv_ratings_reviews;

		$comments = array();
		$post     = get_post( $post );

		if ( $gv_ratings_reviews->component_instances['post-bridge']->name === get_post_type( $post ) ) {
			$comments = get_comments(
				array(
					'post_id' => $post->ID,
					# 'type__in' => array( 'gravityview' ), // TODO: Consider converting to only fetch GravityView comments
					'status' => $status, // Only get approved comments
				)
			);
		}

		return $comments;
	}

	/**
	 * Get the comment data connected to a post bridge
	 *
	 * @param int $bridge_post_id ID of the Post Bridge post
	 * @param string $status Status for reviews. Default: approve
	 * @param boolean $add_metadata Whether to include comment meta. Default: true
	 *
	 * @return array Array of comments with metadata as well
	 */
	public static function get_reviews_data( $bridge_post_id, $status = 'approve', $add_metadata = true ) {

		$comments = self::get_reviews( $bridge_post_id, $status );

		$comments_dump = array();

		foreach ( $comments as $key => $comment ) {
			$data = array();

			foreach ( $comment as $key => $value ) {
				$data[ str_replace( 'comment_', '', $key ) ] = $value;
			}

			if( $add_metadata ) {
				$data['meta'] = get_comment_meta( $comment->comment_ID );
			}

			$comments_dump[] = $data;
		}

		return $comments_dump;
	}

	/**
	 * Retrieve the amount of reviews an entry has. Post being passed should be
	 * the post bridge.
	 *
	 * @since 0.1.0
	 *
	 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
	 *
	 * @return int
	 */
	public static function get_reviews_number( $post ) {
		$reviews     = self::get_reviews( $post );
		$parent_only = array_filter( $reviews, array( __CLASS__, 'filter_parent' ) );

		return count( $parent_only );
	}

	/**
	 * Callback for array_filter to filter comment_parent equal to zero, as non-zero
	 * comment_parent is a review's comment.
	 *
	 * @since 0.1.0
	 *
	 * @param  object $comment
	 * @return bool
	 */
	public static function filter_parent( $comment ) {
		return ! $comment->comment_parent;
	}

	/**
	 * Display the language string for the number of reviews the current entry has.
	 *
	 * @since 0.1.0
	 *
	 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
	 * @param string      $zero Optional. Text for no comments. Default false.
	 * @param string      $one  Optional. Text for one comment. Default false.
	 * @param string      $more Optional. Text for more than one comment. Default false.
	 *
	 * @return string
	 */
	public static function get_reviews_number_text( $post, $zero = false, $one = false, $more = false ) {
		$number = self::get_reviews_number( $post );

		if ( $number > 1 ) {
			$output = str_replace( '%', number_format_i18n( $number ), ( false === $more ) ? __( '% Reviews', 'gravityview-ratings-reviews' ) : $more );
		} elseif ( 0 === intval( $number ) ) {
			$output = ( false === $zero ) ? __( 'No Reviews', 'gravityview-ratings-reviews' ) : $zero;
		} else { // must be one
			$output = ( false === $one ) ? __( '1 Review', 'gravityview-ratings-reviews' ) : $one;
		}

		/**
		 * Filter the comments count for display.
		 *
		 * @since 0.1.0
		 *
		 * @param string $output A translatable string formatted based on whether the count
		 *                       is equal to 0, 1, or 1+.
		 * @param int    $number The number of entry reviews.
		 */
		return apply_filters( 'gv_reviews_number', $output, $number );
	}

	/**
	 * Display the language string for the number of comments the current post has.
	 *
	 * @since 0.1.0
	 *
	 * @param int|WP_Post $post_id Optional. Post ID or WP_Post object. Default is global $post.
	 * @param string $zero       Optional. Text for no comments. Default false.
	 * @param string $one        Optional. Text for one comment. Default false.
	 * @param string $more       Optional. Text for more than one comment. Default false.
	 */
	public static function the_reviews_number_text( $post, $zero = false, $one = false, $more = false ) {
		echo self::get_reviews_number_text( $post, $zero, $one, $more );
	}

	/**
	 * Output link to entry's reviews.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public static function the_reviews_link() {
		global $gravityview_view, $post;

		$fs = wp_parse_args(
			$gravityview_view->field_data['field_settings'],
			array(
				'no_comment_text'     => __( 'Leave a Review', 'gravityview-ratings-reviews' ),
				'one_comment_text'    => __( '1 Review', 'gravityview-ratings-reviews' ),
				'more_comments_text'  => __( '% Reviews', 'gravityview-ratings-reviews' ),
				'show_average_rating' => true,
			)
		);

		$entry = $gravityview_view->field_data['entry'];
		$field = $gravityview_view->field_data['field'];
		$type  = $gravityview_view->atts['entry_review_type'];

		// Link to entry detail.
		$href = GravityView_API::entry_link( $entry, $field );

		// Post ID that links entry with comments.
		$post_bridge_id = self::get_post_bridge_id( $entry['id'] );

		// Replaces current post with bridge post.
		$post = get_post( $post_bridge_id );
		if ( $post && ! is_wp_error( $post ) ) {
			setup_postdata( $post );

			$average = self::get_review_average_rating( $post );
			if ( $fs['show_average_rating'] && ! empty( $average['total_voters'] ) ) {

				if ( 'vote' === $type ) {
					self::the_vote_rating(
						array(
							'rating' => $average['average_vote'],
							'number' => $average['total_voters'],
						)
					);
				} else {
					self::the_star_rating(
						array(
							'rating' => $average['average_stars'],
							'type'   => 'rating',
							'number' => $average['total_voters'],
						)
					);
				}
			}

			echo '<a href="' . esc_url( $href ) . '#gv-entry-reviews"';

			if ( ! empty( $css_class ) ) {
				echo ' class="' . $css_class . '" ';
			}

			echo ' title="' . esc_attr( __( 'Reviews of this entry', 'gravityview-ratings-reviews' ) ) . '">';
			self::the_reviews_number_text( $post, $fs['no_comment_text'], $fs['one_comment_text'], $fs['more_comments_text'] );
			echo '</a>';
		}
		wp_reset_postdata();

	}

	/**
	 * Get review permalink. If the comment is not a review, then original
	 * location will be returned.
	 *
	 * This is used by 'review' component to get proper review permalink
	 * and after-saved redirect.
	 *
	 * When `is_admin()` the permalink will be set to view entry page.
	 *
	 * @since 0.1.0
	 *
	 * @see GravityView_Ratings_Reviews_Review::redirect_to_entry
	 * @see GravityView_Ratings_Reviews_Review::get_comment_link
	 *
	 * @param string $location Original location
	 * @param object $review   Comment object
	 *
	 * @return string
	 */
	public static function get_review_permalink( $location, $review ) {
		global $gv_ratings_reviews, $gravityview_view, $post;

		$current_post = $post;
		$post_id      = $review->comment_post_ID;
		$post_bridge  = $gv_ratings_reviews->component_instances['post-bridge'];
		$review_comp  = $gv_ratings_reviews->component_instances['review'];

		if ( $post_bridge->name === get_post_type( $post_id ) ) {
			$entry_id = absint( get_post_meta( $post_id, $post_bridge->entry_id_meta_key, true ) );

			if ( is_admin() ) {
				$location = self::get_entry_admin_url( $entry_id );
			} else {
				$expected_post_types = array( 'gravityview', $post_bridge->name );

				//
				// When POSTing comment, global $post is expected to be a post bridge.
				// However, the post can be a View, a post bridge, or a post that contains
				// gravityview shortcode. So we need to check shortcode container first,
				// otherwise it's regular View or post bridge.
				//
				// The reason we're doing this is that we need to give a proper redirect
				// location and/or comment permalink if the View is rendered via shortcode.
				//
				if ( ! empty( $_POST[ $review_comp->field_post_container_id ] ) && ! in_array( get_post_type( $_POST[ $review_comp->field_post_container_id ] ), $expected_post_types ) ) {
					$passed_post_id = intval( $_POST[ $review_comp->field_post_container_id ] );
				} else if ( ! empty( $_POST[ $review_comp->field_view_id ] ) ) {
					$passed_post_id = intval( $_POST[ $review_comp->field_view_id ] );
				} else if ( ! empty( $gravityview_view ) ) {
					$passed_post_id = intval( $gravityview_view->id );
				}

				if( empty( $passed_post_id ) ) {
					do_action( 'gravityview_log_debug', __METHOD__ . ' - There was no passed post ID', $review );
					return NULL;
				}

				$entry    = gravityview_get_entry( $entry_id, true );

				$location = gv_entry_link( $entry, $passed_post_id ) . '#review-' . $review->comment_ID;

			}
		}

		return $location;
	}

	/**
	 * Retrieve edit review link.
	 *
	 * @since 0.1.0
	 *
	 * @param int   $comment_id Optional. Comment ID.
	 * @param array $args       Optional. Args to be passed as query string
	 *
	 * @return string
	 */
	public static function get_edit_review_link( $comment_id = 0, $args = array() ) {
		$comment = get_comment( $comment_id );

		if ( ! current_user_can( 'edit_comment', $comment->comment_ID ) ) {
			return;
		}

		$location = admin_url( 'comment.php?action=editcomment&amp;c=' ) . $comment->comment_ID;

		if ( ! empty( $args ) ) {
			$location = add_query_arg( $args, $location );
		}

		/**
		 * Filter the comment edit link.
		 *
		 * @param string $location The edit link.
		 */
		return apply_filters( 'get_edit_comment_link', $location );
	}

	/**
	 * Display edit review link with formatting.
	 *
	 * @since 0.1.0
	 *
	 * @param string $text   Optional. Anchor text.
	 * @param string $before Optional. Display before edit link.
	 * @param string $after  Optional. Display after edit link.
	 * @param array  $args   Optional. Args to be passed as query string
	 *
	 * @return void
	 */
	public static function edit_review_link( $text = null, $before = '', $after = '', $args = array() ) {
		global $comment;

		if ( ! current_user_can( 'edit_comment', $comment->comment_ID ) ) {
			return;
		}

		if ( null === $text ) {
			$text = __( 'Edit This', 'gravityview-ratings-reviews' );
		}

		$link = '<a class="comment-edit-link" href="' . self::get_edit_review_link( $comment->comment_ID, $args ) . '">' . $text . '</a>';

		/**
		 * Filter the comment edit link anchor tag.
		 *
		 * @since 0.1.0
		 *
		 * @param string $link       Anchor tag for the edit link.
		 * @param int    $comment_id Comment ID.
		 * @param string $text       Anchor text.
		 */
		echo $before . apply_filters( 'edit_comment_link', $link, $comment->comment_ID, $text ) . $after;
	}

	/**
	 * Get GF view entry admin URL.
	 *
	 * @since 0.1.0
	 *
	 * @param int $entry_id GF entry ID
	 * @param string $mode  Screen mode. Can be 'edit'.
	 *
	 * @return string URL to view entry in admin
	 */
	public static function get_entry_admin_url( $entry_id, $mode = '' ) {

		$form  = gravityview_get_form_from_entry_id( $entry_id );

		if ( ! in_array( $mode, array( 'edit', ) ) ) {
			$mode = '';
		}

		return admin_url(
			sprintf(
				'admin.php?page=%s&view=%s&id=%d&lid=%d&screen_mode=%s',
				'gf_entries',
				'entry',
				absint( $form['id'] ),
				absint( $entry_id ),
				$mode
			)
		);
	}

	/**
	 * Retrieve reviews. Post being passed should be the post bridge.
	 *
	 * @since 0.1.0
	 *
	 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
	 *
	 * @return array
	 */
	public static function get_review_average_rating( $post = 0 ) {
		global $wpdb, $gv_ratings_reviews;

		//
		// The structure of the result.
		//
		// The suffix should matches with 'Review type'
		// found in 'Ratings and Reviews' meta box.
		//
		$result = array(
			'detail_stars'  => array_fill( 1, 5, 0 ),
			'detail_vote'   => array( 'down' => 0, 'up' => 0 ),
			'average_stars' => 0,
			'average_vote'  => 0,
			'total_voters'  => 0, // Doesn't count someone that doesn't leave a rate.
		);

		$post_bridge_comp = $gv_ratings_reviews->component_instances['post-bridge'];
		$review_comp      = $gv_ratings_reviews->component_instances['review'];

		$post = get_post( $post );
		if ( $post_bridge_comp->name !== get_post_type( $post ) ) {
			return $result;
		}

		// Checks if results are available from cache.
		$last_changed = wp_cache_get( 'last_changed', 'comment' );
		if ( ! $last_changed ) {
			$last_changed = microtime();
			wp_cache_set( 'last_changed', $last_changed, 'comment' );
		}
		$cache_key = "gv_ratings_reviews:get_review_average_rating:$post->ID:$last_changed";

		if ( $cache = wp_cache_get( $cache_key ) ) {
			return $cache;
		}

		// TODO: Convert away from SQL
		$rows = $wpdb->get_results(
			$wpdb->prepare(
				"
				SELECT
					cm.meta_value as star,
					COUNT(cm.meta_value) as count FROM $wpdb->comments c
				LEFT JOIN $wpdb->posts p ON c.comment_post_ID = p.ID
				LEFT JOIN $wpdb->commentmeta cm ON cm.comment_id = c.comment_ID
				WHERE
					c.comment_approved = '1'
					AND
					p.post_type = %s
					AND
					p.ID = %d
					AND
					p.post_status = 'publish'
					AND
					c.comment_parent = 0
					AND
					cm.meta_key = 'gv_review_rate'
				GROUP BY cm.meta_value
				",
				$post_bridge_comp->name,
				$post->ID
			),
			ARRAY_A
		);

		$total_stars = 0;
		$total_votes = 0;
		$total_count = 0;
		foreach ( $rows as $row ) {
			$star  = intval( $row['star'] );
			$count = intval( $row['count'] );
			$vote  = self::get_vote_from_star( $star );

			$result['detail_stars'][ $star ] += $count;

			if ( 1 === $vote ) {
				$result['detail_vote']['up'] += $count;
			} else if ( -1 === $vote ) {
				$result['detail_vote']['down'] += $count;
			}

			$total_count += $count;
			$total_stars += $star * $count;
			$total_votes += $vote * $count;
		}
		$result['average_stars'] = $total_count > 0 ? $total_stars / $total_count : 0;
		$result['average_vote']  = $total_votes;
		$result['total_voters']  = $total_count;

		wp_cache_add( $cache_key, $result );

		return $result;
	}

	/**
	 * Output a complete review form for use within a template.
	 *
	 * This is heavily borrowed from core's comment-template.php
	 *
	 * Most strings and form fields may be controlled through the $args array passed
	 * into the function, while you may also choose to use the comment_form_default_fields
	 * filter to modify the array of default fields if you'd just like to add a new
	 * one or remove a single field. All fields are also individually passed through
	 * a filter of the form comment_form_field_$name where $name is the key used
	 * in the array of fields.
	 *
	 * @since 0.1.0
	 *
	 * @param array       $args {
	 *     Optional. Default arguments and form fields to override.
	 *
	 *     @type array $fields {
	 *         Default comment fields, filterable by default via the 'comment_form_default_fields' hook.
	 *
	 *         @type string $author Comment author field HTML.
	 *         @type string $email  Comment author email field HTML.
	 *         @type string $url    Comment author URL field HTML.
	 *     }
	 *     @type string $comment_field        The comment textarea field HTML.
	 *     @type string $must_log_in          HTML element for a 'must be logged in to comment' message.
	 *     @type string $logged_in_as         HTML element for a 'logged in as <user>' message.
	 *     @type string $comment_notes_before HTML element for a message displayed before the comment form.
	 *                                        Default 'Your email address will not be published.'.
	 *     @type string $comment_notes_after  HTML element for a message displayed after the comment form.
	 *                                        Default 'You may use these HTML tags and attributes ...'.
	 *     @type string $id_form              The comment form element id attribute. Default 'commentform'.
	 *     @type string $id_submit            The comment submit element id attribute. Default 'submit'.
	 *     @type string $name_submit          The comment submit element name attribute. Default 'submit'.
	 *     @type string $title_reply          The translatable 'reply' button label. Default 'Leave a Reply'.
	 *     @type string $title_reply_to       The translatable 'reply-to' button label. Default 'Leave a Reply to %s',
	 *                                        where %s is the author of the comment being replied to.
	 *     @type string $cancel_reply_link    The translatable 'cancel reply' button label. Default 'Cancel reply'.
	 *     @type string $label_submit         The translatable 'submit' button label. Default 'Post a comment'.
	 *     @type string $format               The comment form format. Default 'xhtml'. Accepts 'xhtml', 'html5'.
	 * }
	 * @param int|WP_Post $post_id Post ID or WP_Post object to generate the form for. Default current post.
	 */
	public static function review_form( $args = array(), $post_id = null ) {
		global $gravityview_view, $gv_ratings_reviews, $gv_view_post_container_id;

		if ( null === $post_id ) {
			$post_id = get_the_ID();
		}

		$commenter     = wp_get_current_commenter();
		$user          = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : '';

		$args = wp_parse_args( $args );
		if ( ! isset( $args['format'] ) ) {
			$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
		}

		$req      = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$html5    = 'html5' === $args['format'];
		$fields   =  array(
			'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'gravityview-ratings-reviews' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
			            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
			'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'gravityview-ratings-reviews' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
			            '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
			'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website', 'gravityview-ratings-reviews' ) . '</label> ' .
			            '<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
		);

		$required_text = sprintf( ' ' . __( 'Required fields are marked %s', 'gravityview-ratings-reviews' ), '<span class="required">*</span>' );

		if ( ! empty( $gravityview_view->entries[0] ) ) {
			// gv_entry_link is checking global post, so we need to reset the
			// global post to the view container.
			global $post;

			$current_post = $post;
			$post         = get_post( $gv_view_post_container_id );
			setup_postdata( $post );

			$permalink = gv_entry_link( $gravityview_view->entries[0] );
			$post      = $current_post;
			setup_postdata( $post );
		} else {
			$permalink = apply_filters( 'the_permalink', get_permalink( $post_id ) );
		}

		/**
		 * Filter the default comment form fields.
		 *
		 * @since 0.1.0
		 *
		 * @param array $fields The default comment fields.
		 */
		$fields = apply_filters( 'comment_form_default_fields', $fields );
		$defaults = array(
			'fields'                => $fields,
			'comment_field'         => '<p class="comment-form-comment"><label for="comment">' . _x( 'Review', 'noun', 'gravityview-ratings-reviews' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
			/** This filter is documented in wp-includes/link-template.php */
			'must_log_in'           => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'gravityview-ratings-reviews' ), wp_login_url( $permalink ) ) . '</p>',
			/** This filter is documented in wp-includes/link-template.php */
			'logged_in_as'          => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'gravityview-ratings-reviews' ), get_edit_user_link(), $user_identity, wp_logout_url( $permalink ) ) . '</p>',
			'comment_notes_before'  => '<p class="comment-notes">' . __( 'Your email address will not be published.', 'gravityview-ratings-reviews' ) . ( $req ? $required_text : '' ) . '</p>',
			'comment_notes_after'   => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'gravityview-ratings-reviews' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
			'id_form'               => 'commentform',
			'id_submit'             => 'submit',
			'name_submit'           => 'submit',
			'title_reply'           => __( 'Review this entry', 'gravityview-ratings-reviews' ),
			'title_reply_to'        => __( 'Reply to %s', 'gravityview-ratings-reviews' ),
			'cancel_reply_link'     => __( 'Cancel reply', 'gravityview-ratings-reviews' ),
			'label_submit'          => __( 'Post Review', 'gravityview-ratings-reviews' ),
			'format'                => 'xhtml',

			/** The message shown to users who try to add two reviews to the same entry. */
			'limited_to_one_review' => '<p class="limited-to-one-review">' . sprintf( __( 'You have already reviewed this entry.', 'gravityview-ratings-reviews' ) ) . '</p>',
		);

		/**
		 * Filter the comment form default arguments.
		 *
		 * Use 'comment_form_default_fields' to filter the comment fields.
		 *
		 * @since 0.1.0
		 *
		 * @param array $defaults The default comment form arguments.
		 */
		$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

		/**
		 * Filter review's form template.
		 *
		 * @since 0.1.0
		 *
		 * @param string $template The template file path.
		 */
		require_once apply_filters( 'gv_ratings_reviews_review_form', $gv_ratings_reviews->templates_dir . 'review-form.php' );
	}

	/**
	 * Returns post ID that links entry with comments.
	 *
	 * @since 0.1.0
	 *
	 * @param  mixed $entry_id GF Entry ID
	 * @return mixed
	 */
	public static function get_post_bridge_id( $entry_id = null ) {
		global $gravityview_view;

		if ( null === $entry_id
			&& 'single' === $gravityview_view->context
			&& ! empty( $gravityview_view->entries[0] ) ) {

			$entry_id = $gravityview_view->entries[0]['id'];
		}

		return gform_get_meta( $entry_id, 'gf_entry_to_comments_post_id' );
	}

	/**
	 * Get the titles for the star ratings
	 *
	 * Users may want to modify the rating structure
	 *
	 * @param  int|null $number If defined, the numeric value of the star to get the rating for (1-5)
	 * @return string|array         If $number is defined, the title for a star rating with the value of $number. Otherwise, array of all ratings.
	 */
	private static function get_star_rating_title( $number = null ) {
		$original_star_titles = array(
			1 => _x( '1 star', 'Rating description shown when hovering over a star', 'gravityview-ratings-reviews' ),
			2 => _x( '2 stars', 'Rating description shown when hovering over a star', 'gravityview-ratings-reviews' ),
			3 => _x( '3 stars', 'Rating description shown when hovering over a star', 'gravityview-ratings-reviews' ),
			4 => _x( '4 stars', 'Rating description shown when hovering over a star', 'gravityview-ratings-reviews' ),
			5 => _x( '5 stars', 'Rating description shown when hovering over a star', 'gravityview-ratings-reviews' ),
		);

		/**
		 * Filter the star rating hover titles.
		 *
		 * Make sure to keep the array key numbers intact; they map with the star rating.
		 *
		 * You can set an empty string to disable the title attribute from appearing in GravityView_Ratings_Reviews_Helper::get_star_rating()
		 *
		 * @link https://gist.github.com/zackkatz/0f160dd235049b59f775 Example filter use
		 *
		 * @since 0.1.1
		 *
		 * @param array $stars The array of star ratings
		 */
		$star_titles = apply_filters( 'gv_ratings_reviews_star_rating_titles', $original_star_titles );

		// If number is defined, return the title for that number
		if ( $number ) {

			if ( isset( $star_titles[ $number ] ) ) {
				return $star_titles[ $number ];
			} elseif ( isset( $original_stars[ $number ] ) ) {
				return $original_stars[ $number ];
			} else {
				return null;
			}
		}

		// Otherwise, return the whole array
		return $star_titles;
	}

	/**
	 * Get a HTML element with a star rating for a given rating. Copied from
	 * wp_star_rating with a bit of sugar addition.
	 *
	 * @since 0.1.0
	 *
	 * @param array $args {
	 *     Optional. Array of star ratings arguments.
	 *
	 *     @type int    $rating       The rating to display, expressed in either a 0.5 rating increment,
	 *                                or percentage. Default 0.
	 *     @type string $type         Format that the $rating is in. Valid values are 'rating' (default),
	 *                                or, 'percent'. Default 'rating'.
	 *     @type int    $number       The number of ratings that makes up this rating. Default 0.
	 *     @type bool   $clickable    Whether the star is clickable or not. Default false.
	 *     @type bool   $display_text Whether to show the text. Default false.
	 * }
	 *
	 * @return string
	 */
	public static function get_star_rating( $args = array() ) {
		$defaults = array(
			'rating'       => 0,
			'type'         => 'rating',
			'number'       => 0,
			'clickable'    => false,
			'display_text' => false,
		);
		$r = wp_parse_args( $args, $defaults );

		// Non-english decimal places when the $rating is coming from a string.
		$rating = str_replace( ',', '.', $r['rating'] );

		// Convert Percentage to star rating, 0..5 in .5 increments
		if ( 'percent' == $r['type'] ) {
			$rating = round( $rating / 10, 0 ) / 2;
		}

		// Calculate the number of each type of star needed
		$full_stars = floor( $rating );
		$half_stars = ceil( $rating - $full_stars );
		$empty_stars = 5 - $full_stars - $half_stars;

		if ( $r['number'] ) {
			/* translators: 1: The rating, 2: The number of ratings */
			$format = _n( '%1$s rating based on %2$s rating', '%1$s rating based on %2$s ratings', $r['number'], 'gravityview-ratings-reviews' );
			$title = sprintf( $format, number_format_i18n( $rating, 1 ), number_format_i18n( $r['number'] ) );
		} else {
			/* translators: 1: The rating */
			$title = sprintf( __( '%s rating', 'gravityview-ratings-reviews' ), number_format_i18n( $rating, 1 ) );
		}

		$output = '';
		if ( $r['clickable'] ) {
			$output .= '<div class="gv-star-rating gv-star-rate-holder">';

			$format = '<span class="gv-star-rate"%s>&#61780</span>';

			$star_count = 1;
			while ( $star_count <= 5 ) {

				$title = self::get_star_rating_title( $star_count );

				// You can set an empty string to disable the title attribute.
				if ( ! empty( $title ) ) {
					$title = ' title="'. esc_attr( $title ).'"';
				}

				$output .= sprintf( $format, $title );

				$star_count++;
			}

			$output .= '</div>';
		} else {

			// If the number is passed, this is displaying the aggregate rating
			if ( $r['number'] ) {
				$output .= '<div class="gv-star-rating" title="' . esc_attr( $title ) . '" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';
				$output .= '<meta itemprop="reviewCount" content="'.$r['number'].'">';
			}
			// Otherwise, just a single rating
			else {
				$output .= '<div class="gv-star-rating" title="' . esc_attr( $title ) . '" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">';
			}

			$output .= '<span class="screen-reader-text">' . $title . '</span>';
			$output .= '<meta itemprop="ratingValue" content="'.round( $rating, 3 ).'">';
			$output .= str_repeat( '<span class="gv-star gv-star-full"></span>', $full_stars );
			$output .= str_repeat( '<span class="gv-star gv-star-half"></span>', $half_stars );
			$output .= str_repeat( '<span class="gv-star gv-star-empty"></span>', $empty_stars );
			$output .= '</div>';
		}

		if ( $r['display_text'] ) {
			$output .= '<div class="gv-star-rating-text">' . esc_html( $title ) . '</div>';
		}

		/**
		 * Filter for HTML element with a star rating.
		 *
		 * @param string $output HTML element with a star rating
		 *
		 * @param  array $r Optional array of star ratings arguments.
		 *
		 * @since 0.1.0
		 */
		return apply_filters( 'gv_ratings_reviews_star_rating', $output, $r );
	}

	/**
	 * Outputs a HTML element with the star rating exposed on a 0..5 scale in
	 * half star increments (ie. 1, 1.5, 2 stars). Optionally, if specified, the
	 * number of ratings may also be displayed by passing the $number parameter.
	 *
	 * @since 0.1.0
	 *
	 * @param array $args {
	 *     Optional. Array of star ratings arguments.
	 *
	 *     @type int    $rating       The rating to display, expressed in either a 0.5 rating increment,
	 *                                or percentage. Default 0.
	 *     @type string $type         Format that the $rating is in. Valid values are 'rating' (default),
	 *                                or, 'percent'. Default 'rating'.
	 *     @type int    $number       The number of ratings that makes up this rating. Default 0.
	 *     @type bool   $clickable    Whether the star is clickable or not. Default false.
	 *     @type bool   $display_text Whether to show the text. Default false.
	 * }
	 *
	 * @return void
	 */
	public static function the_star_rating( $args = array() ) {
		echo self::get_star_rating( $args );
	}

	/**
	 * Get the text used to display positive and negative ratings
	 *
	 * @param boolean $type Use "up" for positive or "down" for negative. Up default.
	 *
	 * @return string "+1" and "-1" are defaults
	 */
	public static function get_vote_rating_text( $rating_value = 1, $positive = true ) {

		$rating_abs = absint( $rating_value );

		$original_rating_text = array(
			'up' => sprintf( __( '+%d', 'gravityview-ratings-reviews' ), number_format_i18n( $rating_abs ) ),
			'down' => sprintf( __( '-%d', 'gravityview-ratings-reviews' ), number_format_i18n( $rating_abs ) ),
			'zero' => __( 'No Rating', 'gravityview-ratings-reviews' ),
		);

		/**
		 * Filter the vote rating text.
		 *
		 * Make sure to keep the array key numbers intact; they map with the vote ratings.
		 *
		 * @param array $original_rating_text {
		 *      @type string    $up    The text to show when the rating is positive. Default: "+1"
		 *      @type string    $down  The text to show when the rating is negative. Default: "-1"
		 *      @type string    $down  The text to show when there is no rating. Default: "No Rating"
		 * }
		 */
		$rating_text = apply_filters( 'gv_ratings_reviews_vote_rating_text', $original_rating_text );

		if( $rating_value > 0 ) {
			return $rating_text['up'];
		} else if( $rating_value < 0 ) {
			return $rating_text['down'];
		} else {
			return $rating_text['zero'];
		}
	}

	/**
	 * Get a vote HTML element.
	 *
	 * @since 0.1.0
	 *
	 * @param array $args {
	 *     Optional. Array of star ratings arguments.
	 *
	 *     @type int    $rating       The rating to display. Default 0.
	 *     @type int    $number       The number of ratings that makes up this rating. Default 0.
	 *     @type bool   $clickable    Whether the vote is clickable or not. Default false.
	 *     @type bool   $display_text Whether to show the text. Default false.
	 * }
	 *
	 * @return string
	 */
	public static function get_vote_rating( $args = array() ) {
		$defaults = array(
			'rating'       => 0,
			'number'       => 0,
			'clickable'    => false,
			'display_text' => false,
		);
		$r = wp_parse_args( $args, $defaults );

		$is_positive_rating = ( $r['rating'] > 0 );
		$is_negative_rating = ( $r['rating'] < 0 );

		$r['rating'] = intval( $r['rating'] );
		$r['number'] = intval( $r['number'] );

		if ( $r['number'] ) {
			$rating_string = self::get_vote_rating_text( $r['rating']  );

			/* translators: 1: The rating, 2: The number of ratings */
			$format = _n( '%1$s rating based on %2$s vote', '%1$s rating based on %2$s votes', $r['number'], 'gravityview-ratings-reviews' );
			$title = sprintf( $format, $rating_string, number_format_i18n( $r['number'] ) );
		} else {
			/* translators: 1: The rating */
			$title = sprintf( __( '%s rating', 'gravityview-ratings-reviews' ), number_format_i18n( $r['rating'] ) );
		}

		$output = '';
		if ( $r['clickable'] ) {
			$output_wrapper = '<div class="gv-vote-rating gv-vote-rate-holder">';
			$vote_tag = 'a';
		} else {
			$output_wrapper = '<div class="gv-vote-rating" title="' . esc_attr( $title ) . '">';
			$vote_tag = 'span';
		}

		$output .= $output_wrapper;
			$output .= sprintf(
				'<%s class="gv-vote-up%s" title="%s"></%1$s>',
				$vote_tag,
				$is_positive_rating ? ' gv-rate-mutated' : '',
				$is_positive_rating ? esc_attr__( 'Up vote shows the entry is useful for the author', 'gravityview-ratings-reviews' ) : ''
			);
			$output .= sprintf(
				'<%s class="gv-vote-down%s" title="%s"></%1$s>',
				$vote_tag,
				$is_negative_rating ? ' gv-rate-mutated' : '',
				$is_negative_rating ? esc_attr__( 'Down vote shows the entry is not useful for the author', 'gravityview-ratings-reviews' ) : ''
			);
			$output .= sprintf(
				'<span class="gv-vote-rating-text" title="%s">%s</span>',
				esc_attr( $title ),
				esc_html( self::get_vote_rating_text( $r['rating'] ) )
			);
		$output .= '</div>';

		// If there is more than one rating, display the rating average
		if ( $r['number'] > 1 && $r['display_text'] ) {
			$output .= sprintf( '<div class="gv-vote-average-rating">%s</div>', $title );
		}

		/**
		 * Filter for HTML element with a vote rating.
		 *
		 * @param string $output HTML element with a vote rating
		 * @param array  $r Array of args used to generate the output
		 *
		 * @since 0.1.0
		 */
		return apply_filters( 'gv_ratings_reviews_vote_rating', $output, $r );
	}

	/**
	 * Outputs a HTML element for vote rating.
	 *
	 * @since 0.1.0
	 *
	 * @param array $args {
	 *     Optional. Array of star ratings arguments.
	 *
	 *     @type int    $rating       The rating to display. Default 0.
	 *     @type int    $number       The number of ratings that makes up this rating. Default 0.
	 *     @type bool   $clickable    Whether the star is clickable or not. Default false.
	 *     @type bool   $display_text Whether to show the text. Default false.
	 * }
	 *
	 * @return void
	 */
	public static function the_vote_rating( $args = array() ) {
		echo self::get_vote_rating( $args );
	}

	/**
	 * Get vote value from a given star rating.
	 *
	 * @since 0.1.0
	 *
	 * @param int $rating Star rating, from 0 - 5
	 *
	 * @return int Valid value: -1, 0, +1
	 */
	public static function get_vote_from_star( $rating ) {
		$rating = absint( $rating );

		if ( 3 > $rating && 0 < $rating ) {
			return -1;
		} elseif ( 3 < $rating ) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * Get star value from a given vote rating (-1, 0, 1).
	 *
	 * @since 0.1.0
	 *
	 * @param int $vote Vote rating
	 *
	 * @return int
	 */
	public static function get_star_from_vote( $vote ) {
		$vote = intval( $vote );

		if ( 1 === $vote ) {
			return 5;
		} elseif ( -1 === $vote ) {
			return 1;
		} else {
			return 0;
		}
	}

	/**
	 * Get star value from a given vote rating (-1, 0, 1).
	 *
	 * @since 0.1.0
	 *
	 * @param int $vote Vote rating
	 *
	 * @return int[] Array of review ratings
	 */
	public static function get_ratings( $post_id = null, $entry_id = null ) {
		if ( is_null( $post_id ) ){
			$post_id = gform_get_meta( $entry_id, 'gf_entry_to_comments_post_id' );
		}

		if ( ! is_numeric( $post_id ) ){
			return array();
		}

		$query = new WP_Comment_Query( array( 'post_id' => $post_id ) );
		$rates = array();
		foreach ( $query->comments as $comment ){
			$rates[] = get_comment_meta( $comment->comment_ID, 'gv_review_rate', true );
		}
		return $rates;

	}

	/**
	 * Get star value from a given vote rating (-1, 0, 1).
	 *
	 * @since 0.1.0
	 *
	 * @param int $vote Vote rating
	 *
	 * @return int
	 */
	public static function get_ratings_detailed( $post_id = null, $entry_id = null ) {
		$stars = self::get_ratings( $post_id, $entry_id );
		$total_count = count( $stars );

		$count_stars = array_replace( array_fill( 1, 5, 0 ) , array_count_values( $stars ) );
		$total_stars = array_sum( $stars );

		$votes = array();
		foreach ( $stars as $star ) {
			$votes[] = self::get_vote_from_star( $star );
		}
		$count_votes = array_replace( array( '-1' => 0, '0' => 0, '1' => 0, ) , array_count_values( $votes ) );
		$total_votes = array_sum( $votes );

		$metas = array(
			'star_1' => $count_stars[1],
			'star_2' => $count_stars[2],
			'star_3' => $count_stars[3],
			'star_4' => $count_stars[4],
			'star_5' => $count_stars[5],
			'stars' => $total_count > 0 ? $total_stars / $total_count : 0,

			'vote_down' => $count_votes[-1],
			'vote_neutral' => $count_votes[0],
			'vote_up' => $count_votes[1],
			'votes' => $total_count > 0 ? $total_votes : 0,

			'total' => $total_count,
		);

		return $metas;
	}

	/**
	 * If view setting limits to one review per person then the person that
	 * has left review before is not allowed to leave review again.
	 *
	 * @since 0.1.0
	 *
	 * @param  int|WP_Post $post_bridge         The post ID or WP_Post object of post bridge
	 * @param  string      $review_author       Reviewer's name
	 * @param  string      $review_author_email Reviewer's email
	 * @return bool
	 */
	public static function is_user_allowed_to_leave_review( $post_bridge, $review_author = '', $review_author_email = '' ) {
		global $gravityview_view, $gv_ratings_reviews;

		$post_bridge_comp = $gv_ratings_reviews->component_instances['post-bridge'];
		$review_comp      = $gv_ratings_reviews->component_instances['review'];

		// Admins and users who can moderate comments can leave unlimited comments.
		if ( GFCommon::current_user_can_any( array('manage_options', 'moderate_comments') ) ) {
			return true;
		}

		$is_allowed = true;

		$post_bridge = get_post( $post_bridge );
		if ( $post_bridge_comp->name === get_post_type( $post_bridge ) ) {
			$settings = array();
			//
			// When POSTing data to wp-comments-post.php, the global `$gravityview_view`
			// is not instantiated.
			//
			if ( empty( $gravityview_view ) ) {
				if ( ! empty( $_POST[ $review_comp->field_view_id ] ) ) {
					$settings = gravityview_get_template_settings( $_POST[ $review_comp->field_view_id ] );
				} else {
					// This could be when admin replies from admin dashboard.
					// We don't have View object at this point so that we ignore
					// settings.
					$settings['limit_one_review_per_person'] = false;
				}
			} else {
				$settings = $gravityview_view->atts;
			}

			if ( ! $settings['limit_one_review_per_person'] ) {
				return $is_allowed;
			}

			$reviewdata = array(
				'comment_post_ID'      => $post_bridge->ID,
				'comment_author'       => $review_author,
				'comment_author_email' => $review_author_email,
			);

			if ( ! empty( $reviewdata['comment_author'] ) && self::is_user_has_left_review_before( $reviewdata ) ) {
				$is_allowed = false;
			}
		}

		return $is_allowed;
	}

	/**
	 * Checks whether user has left a review to entry before.
	 *
	 * @since 0.1.0
	 *
	 * @param  array $commentdata Contains information on the comment
	 * @return bool
	 */
	public static function is_user_has_left_review_before( $commentdata ) {
		global $wpdb;

		// Simple has_review check.
		//
		// Expected_slashed ($comment_post_ID, $comment_author, $comment_author_email).
		// TODO: Convert from SQL to WP_Comment_Query
		$has_review = $wpdb->prepare(
			"
			SELECT comment_ID FROM $wpdb->comments
			WHERE
				comment_post_ID = %d
				AND
				comment_parent = 0
				AND
				comment_approved != 'trash'
				AND ( comment_author = %s ",
			wp_unslash( $commentdata['comment_post_ID'] ),
			wp_unslash( $commentdata['comment_author'] )
		);
		if ( $commentdata['comment_author_email'] ) {
			$has_review .= $wpdb->prepare(
				'OR comment_author_email = %s ',
				wp_unslash( $commentdata['comment_author_email'] )
			);
		}
		$has_review .= ') LIMIT 1';

		if ( $wpdb->get_var( $has_review ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the review list header.
	 *
	 * @since 0.1.0
	 *
	 * @return string
	 */
	public static function get_the_review_list_header() {
		return self::get_list_template_content( 'header' );
	}

	/**
	 * Output the review list header.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public static function the_review_list_header() {
		echo self::get_the_review_list_header();
	}

	/**
	 * Get the review list body.
	 *
	 * @since 0.1.0
	 *
	 * @return string
	 */
	public static function get_review_list_body() {
		return self::get_list_template_content( 'body' );
	}

	/**
	 * Output the review list body.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public static function the_review_list_body() {
		echo self::get_review_list_body();
	}

	/**
	 * Get the review list footer.
	 *
	 * @since 0.1.0
	 *
	 * @return string
	 */
	public static function get_the_review_list_footer() {
		return self::get_list_template_content( 'footer' );
	}

	/**
	 * Output the review list footer.
	 *
	 * @since 0.1.0
	 *
	 * @return void
	 */
	public static function the_review_list_footer() {
		echo self::get_the_review_list_footer();
	}

	/**
	 * Get template content of the review list section.
	 *
	 * @since 0.1.0
	 *
	 * @param  string $section Valid sections are: 'header', 'body', and 'footer'.
	 * @return string
	 */
	protected static function get_list_template_content( $section ) {
		global $gv_ratings_reviews;

		if ( ! in_array( $section, array( 'header', 'body', 'footer' ) ) ) {
			return '';
		}

		ob_start();
		$template = apply_filters( "gv_ratings_reviews_list_{$section}_template", $gv_ratings_reviews->templates_dir . "review-list-{$section}.php" );
		include $template;

		return ob_get_clean();
	}
}
