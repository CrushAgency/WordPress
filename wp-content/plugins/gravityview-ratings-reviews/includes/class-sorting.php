<?php
/**
 * Handle sorting by Ratings Reviews fields
 *
 * @package   GravityView_Ratings_Reviews
 * @license   GPL2+
 * @author    Katz Web Services, Inc.
 * @link      http://gravityview.co
 * @copyright Copyright 2014, Katz Web Services, Inc.
 *
 * @since 0.1.0
 */
class GravityView_Ratings_Reviews_Sorting extends GravityView_Ratings_Reviews_Component {

	public function load() {
		add_filter( 'gravityview_search_criteria', array( $this, 'search_criteria' ), 10, 3 );
	}

	/**
	 * @param $criteria
	 * @param $form_ids
	 * @param $context_view_id
	 *
	 * @return mixed
	 */
	function search_criteria( $criteria, $form_ids, $context_view_id ) {

		// Set `gravityview_ratings_stars` sorting to be numeric in GF
		if( !empty( $criteria['sorting'] ) && $criteria['sorting']['key'] === 'gravityview_ratings_stars' ) {
			$criteria['sorting']['is_numeric'] = true;
		}

		return $criteria;
	}
}