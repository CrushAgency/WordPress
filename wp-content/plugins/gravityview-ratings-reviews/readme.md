# GravityView - Ratings Reviews Extension #
**Tags:** gravityview  
**Requires at least:** 3.6  
**Tested up to:** 4.2.1  
**Stable tag:** trunk  
**Contributors:** katzwebservices, bordoni, akeda  
**License:** GPL 3 or higher  

Enable Ratings and Reviews for entries in GravityView.

## Installation ##

1. Upload plugin files to your plugins folder, or install using WordPress' built-in Add New Plugin installer
2. Activate the plugin
3. Configure "Ratings and Reviews" via meta box in edit View
4. News fields regarding review also appear when configure your View

## Frequently Asked Questions ##

### Why can I leave more than one comment? ###

Administrators, users who are able to moderate comments, and users who have full permission for Gravity Forms functionality are able to leave unlimited comments on ratings.

## Changelog ##

### 1.0.1 beta on May 7 ###
* Added: Export review details (review title, text, and more) in when exporting entries from Gravity Forms
* Added: Support for GravityView 1.8 tabbed settings (coming soon)
* Fixed: Sorting entries by rating

### 1.0 beta ###
* Added: Export Ratings & Votes columns in "Export Entries" screen
* Fixed: Editing Star and Voting ratings when editing a comment in the Admin
* Fixed: Delete Post Bridge when entry is deleted
* Fixed: Only show approved comments
* Fixed: Prevent underlined icons
* Tweak: Comment text sanitization improved
* Modified: Set comment type to `gravityview`


### 0.2 ###
* Only use approved reviews in counts and displayed comments
* Add `gravityview` comment type

### 0.1.0 ###
* Initial release
