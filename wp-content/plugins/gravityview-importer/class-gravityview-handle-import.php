<?php

use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;

class GravityView_Handle_Import {

	var $delimiter;

	/**
	 * @var GravityView_Entry_Importer
	 */
	var $Entry_Importer;

	/**
	 * @var Goodby\CSV\Import\Standard\Lexer
	 */
	var $Lexer;

	/**
	 * @var array
	 */
	var $file = array();

	var $_read_only = false;

	var $field_map = array();

	var $header = array();

	/**
	 * @var bool
	 */
	private $_header_only = false;

	/**
	 * @var int
	 */
	private $_counter = 0;

	/**
	 * @var GV_Import_Entries_Addon
	 */
	var $Addon;

	static $instance;

	private function __construct() {

		$this->Addon = gravityview_importer();

		$this->Entry_Importer = GravityView_Entry_Importer::getInstance();

		$this->add_hooks();
	}

	function add_hooks() {
		add_action('gravityview-importer/import', array( $this, 'gfImport' ) );
	}

	public static function getInstance() {

		if ( empty( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	function setFile( array $file ) {
		$this->file = $this->Addon->get_file();
	}

	function getFilePath() {

		$file = $this->getFileArray();

		return $file ? $file['file'] : NULL;
	}

	/**
	 * @return string|null The mime type of the file. NULL: no file array
	 */
	function getFileType() {

		$file = $this->getFileArray();

		return $file ? strtolower( $file['type'] ) : NULL;
	}

	function getFileArray() {
		return $this->Addon->get_file();
	}

	public function getHeaderRow() {

		// Already set.
		if( $this->header ) {
			return $this->header;
		}

		$this->field_map = $this->Addon->_get_field_map_fields();

		$this->_read_only = true;

		$this->_header_only = true;

		$this->parseFile();

		if( empty( $this->header ) ) {
			return array();
		}

		$this->header = array_values( $this->header );

		return $this->header;
	}

	public function getHeaderRowFieldMap() {

		$headers = $this->getHeaderRow();

		$fields = array();

		$i = 1;
		foreach ( $headers as $header ) {

			// Don't allow for empty labels
			if ( empty( $header ) ) {
				$header = sprintf( __( 'Empty (Column %d)', 'gravityview-importer' ), $i );
			}

			$fields[] = array(
				'name'  => sanitize_title_with_dashes( $header ),
				'label' => $header,
			);

			$i ++;
		}

		return $fields;
	}

	function gfImport() {

		$this->header = $this->getHeaderRow();

		$this->field_map = $this->Addon->_get_field_map_fields();

		$this->_read_only = false;

		$this->_header_only = false;

		$this->parseFile();

	}

	/**
	 * Parse the file
	 */
	function parseFile() {

		$file_path = $this->getFilePath();

		if( empty( $file_path ) ) {
			return;
		}

		// Prevent timeout overall
		gv_importer_reset_time_limits();

		$this->Lexer = new Lexer( $this->getLexerConfig() );

		$Interpreter = new Interpreter;

		$Interpreter->unstrict();

		$lineNumber = -1;

		$Interpreter->addObserver( function( array $row ) use ( &$lineNumber ) {

			$continue_running = GravityView_Handle_Import::getInstance()->parseRow( $row, $lineNumber );

			if( false === $continue_running ) {
				return;
			}

		});

		$this->Lexer->parse( $file_path, $Interpreter );

		// Make sure there are no leftover entries after batches have been processed
		if( ! $this->_header_only ) {
			do_action( 'gravityview-importer/end-of-file', $this, $lineNumber );
		}

	}

	/**
	 *
	 * @param $row
	 * @param $lineNumber
	 * @return boolean Continue running observer?
	 */
	function parseRow( $row, &$lineNumber ) {

		// Reset timeout on each row
		gv_importer_reset_time_limits();

		$lineNumber++;

		// If we're only getting the header...
		if( $this->_header_only ) {
			if( $lineNumber === 0 && empty( $this->header ) ) {
				$this->header = $row;
			}
			return false;
		}

		// Get the IDs for the field mapping (date_created, 4, 5.2, etc)
		$input_ids = array_values( $this->field_map );

		// The arrays should be the same size.
		if( sizeof( $row ) < sizeof( $input_ids ) ) {
			$row = array_pad( $row, sizeof( $input_ids ), '' );
		}

		// The field map will have the same # of items as the CSV itself. We can just map the keys from
		// $input_ids onto the [0],[1],[2], etc. of the CSV row.
		$combined_row = array_combine( $input_ids, $row );

		// Any unmapped columns will have the key of ''. Unset them (only import mapped columns)
		unset( $combined_row[''] );

		do_action( 'gravityview-importer/process-row', $combined_row, $lineNumber );
	}

	/**
	 * Check whether the name of the file passes any charset information.
	 *
	 * This way, users can name a file to include the charset, like:
	 * `example-utf-16.txt` or `example-Windows-1252.csv`
	 *
	 * @link http://docs.gravityview.co/article/258-exporting-a-csv-from-excel
	 * @link http://php.net/manual/en/mbstring.supported-encodings.php
	 *
	 * @param string $name File name
	 *
	 * @return string Charset name, `UTF-8` by default
	 */
	private function getCharsetFromName( $name ) {

		$charsets = array(

			'UCS-4BE',
			'UCS-4LE',

			'UCS-2BE',
			'UCS-2LE',
			'UCS-2',

			'UTF-32BE',
			'UTF-32LE',
			'UTF-32',

			'UTF-16BE',
			'UTF-16LE',
			'UTF-16',

			'UTF7-IMAP',
			'UTF-7',

			'EUC-JP',
			'eucJP-win',

			'SJIS-Mobile#DOCOMO',
			'SJIS-DOCOMO',
			'SJIS-Mobile#KDDI',
			'SJIS-KDDI',
			'SJIS-Mobile#SOFTBANK',
			'SJIS-SOFTBANK',
			'SJIS-win',
			'SJIS-mac',
			'SJIS',

			'ISO-2022-JP-MOBILE#KDDI',
			'ISO-2022-JP-KDDI',
			'ISO-2022-JP-MS',
			'ISO-2022-JP',
			'ISO-2022-KR',

			// Group with longer first so -10 matches before -1
			'ISO-8859-10',
			'ISO-8859-13',
			'ISO-8859-14',
			'ISO-8859-15',
			'ISO-8859-1',
			'ISO-8859-2',
			'ISO-8859-3',
			'ISO-8859-4',
			'ISO-8859-5',
			'ISO-8859-6',
			'ISO-8859-7',
			'ISO-8859-8',
			'ISO-8859-9',

			// Group with longer first so -8-Mobile matches before -8
			'UTF-8-Mobile#DOCOMO',
			'UTF-8-Mobile#KDDI-A',
			'UTF-8-Mobile#KDDI-B',
			'UTF-8-Mobile#SOFTBANK',
			'UTF-8-SOFTBANK',
			'UTF-8-DOCOMO',
			'UTF-8-KDDI',
			'UTF-8',

			'JIS-ms',
			'JIS',

			'CP50220raw',
			'CP50220',
			'CP50221',
			'CP50222',

			'MacJapanese',
			'ASCII',
			'CP932',
			'CP51932',
			'byte2be',
			'byte2le',
			'byte4be',
			'byte4le',
			'BASE64',
			'HTML-ENTITIES',
			'7bit',
			'8bit',
			'EUC-CN',
			'CP936',
			'GB18030',
			'HZ',
			'EUC-TW',
			'CP950',
			'BIG-5',
			'EUC-KR',
			'UHC',
			'CP1251',
			'CP1252',
			'Windows-1252',
			'Windows-1251',
			'CP866',
			'IBM866',
			'KOI8-R',
		);

		/**
		 * Modify the default charset for files
		 * @param string $charset PHP Charset string. Default: `UTF-8`
		 * @param string $name File name
		 */
		$default_charset = apply_filters( 'gravityview-importer/default-charset', 'UTF-8', $name );

		$charsets_quote = array_map( 'preg_quote', $charsets );

		$charsets_string = implode( '|', $charsets_quote );

		preg_match('/'.$charsets_string.'/i', $name, $matches );

		// No charset data was found
		if( empty( $matches[0] ) ) {
			$return = in_array( $default_charset, $charsets ) ? $default_charset : 'UTF-8';
		} else {
			// Charset data was found
			$return = esc_attr( $matches[0] );
		}

		unset( $charsets, $charsets_quote, $charsets_string );

		return $return;
	}

	/**
	 * Get the LexerConfig object with character set, delimiter info (CSV/TSV), and whether to ignore header line
	 *
	 * @return LexerConfig
	 */
	private function getLexerConfig() {

		$config = new LexerConfig;

		$file = $this->getFileArray();

		if( in_array( $file['type'], array( 'text/tab-separated-values', 'tsv', 'text/plain' ) ) ) {

			$config->setDelimiter("\t");
		}

		if( false === $this->_header_only ){
			$config->setIgnoreHeaderLine( true );
		}

		$charset = $this->getCharsetFromName( basename( $file['file'] ) );

		$config->setFromCharset( $charset );

		// Standardize!
		$config->setToCharset('UTF-8');

		do_action( 'gravityview-importer/config', $config );

		return $config;
	}

}