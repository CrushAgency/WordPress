=== GravityView Importer ===
Tags: gravity forms, gravityview
Requires at least: 3.8
Tested up to: 4.2.2
Stable tag: trunk
Contributors: katzwebservices
License: GPL 3 or higher

Import Gravity Forms entries

== Description ==

Easily import Gravity Forms entries from a CSV file. Learn more on [gravityview.co](https://gravityview.co/extensions/gravity-forms-entry-importer/).

== Installation ==

1. Upload plugin files to your plugins folder, or install using WordPress' built-in Add New Plugin installer
2. Activate the plugin
3. Follow the instructions

== Changelog ==

= 1.0.7 on May 12 =
* Fixed: Prevent "Your emails do not match" error when Email field has "Enable Email Confirmation" enabled
* Fixed: Mapping "Created By" was not properly assigning imported entries to the defined user
* Fixed: JSON-formatted Post Image field imports
* Fixed: JSON-formatted Post Tags displaying as JSON in the Gravity Forms Entry
* Fixed: For web hosts without the `mb_convert_encoding()` function, add an alternative
* Fixed: PHP notice related to compatibility with Gravity Forms `get_field_map_choices()` method
* Fixed: Make sure that `__DIR__` is defined on the server

= 1.0.6 on May 4 =
* Fixed: Fatal error during import if a name could not be parsed

= 1.0.5 on April 30 =
* Fixed: "There was a problem while inserting the field values" error on some server configurations
* Updated: Hungarian translation (thanks Robert Tokar!)
* Added: Additional information when displaying an error returned by Gravity Forms
* Fixed: PHP warning caused by CSV parsing library

= 1.0.4 on April 29 =
* Fixed: PHP version 5.3 compatibility

= 1.0.3 on April 29  =
* Added: Support for field Default Values
* Fixed: Name and Address field validation issues
* Fixed: Set width for Field Mapping dropdowns to prevent overflow
* Fixed: Updating Post Data
* Fixed: Show all import-blocking errors for each row in the report, not just one per row
* Fixed: Show better phone formatting error
* Updated translations:
    - Bengali (thanks [@tareqhi](https://www.transifex.com/accounts/profile/tareqhi/)!)
    - Hungarian (thanks [@Darqebus](https://www.transifex.com/accounts/profile/Darqebus/)!)

= 1.0.2 =
* Fixed: Fatal error when handling import in some installations
* Fixed: Set max width for drop-downs in Conditional Logic section
* Updated: Translations

= 1.0.1 Beta =
* Allow for changing character set of imported file ([read how](http://docs.gravityview.co/article/258-exporting-a-csv-from-excel#charset))
* Fixed PHP notices and a fatal error
* Don't show "Download File with Errors" button when there are no added entries
* Fix support for TSV files, allow Text files

= 1.0 Beta =

* First preview release

== Upgrade Notice ==