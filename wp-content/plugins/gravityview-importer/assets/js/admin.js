/**
 * globals jQuery,gv_importer_strings
 */

(function( $ ) {
	'use strict';

	var GVImport = {

		selectFields: null,

		init: function() {

			GVImport.setVars();
			GVImport.moveButtons();
			GVImport.updateFormParameters();

			$( document )
				.on('change', '#gform-settings', GVImport.updateAvailableFields )
				.on('submit', '#gform-settings', GVImport.submitSettings )
				.on('click', GVImport.resetButton, GVImport.resetMapping )
				.on('click', GVImport.smartButton, GVImport.smartMapping )
				.on('click', ':input[type=submit]', GVImport.addClickedAttr )
				.on('click', '.gv-importer-hide-console', GVImport.toggleConsole )
				.on('click', '.gv-complete-import', GVImport.completeImport );

			// Force available fields to be triggered
			$( '#gform-settings' ).trigger('change');
		},

		/**
		 * The reset button was inside the tooltip. This fixes that.
		 */
		moveButtons: function() {

			$( '#gaddon-setting-row-import_field_map' ).find('th button' ).appendTo($( '#gaddon-setting-row-import_field_map > th' ));
		},

		/**
		 * Set initial values if the fields are exact matches
		 *
		 * @param e
		 */
		smartMapping: function( e ) {
			e.preventDefault();

			$('.settings-field-map-table').find('tr')
				.filter(function() {
					return $('select', $(this) ).val() === '';
				})
				.each(function() {
					var column_label = $('td:first-child label', $( this ) ).text().trim().toLowerCase();
					$( 'option', $( 'td:last-child select', $( this ) ) ).filter(function() {
						return $(this ).text().trim().toLowerCase() === column_label;
					} ).attr('selected', 'selected');
				});
		},

		/**
		 * Reset the field mapping to empty values
		 * @param e
		 */
		resetMapping: function( e ) {
			// We don't want the form reset, just the fields we want.
			// Also causes weird conflicts with GVImport.updateAvailableFields
			e.preventDefault();

			$( '.settings-field-map-table select' ).val( '' );

			// Reset the disabled attr
			$( '#gform-settings' ).trigger( 'change' );

			$( this ).fadeOut('fast');
		},

		/**
		 * Add click attr to the clicked submit button to allow submitSettings to see what submit button was clicked
		 * @param e
		 */
		addClickedAttr: function( e ) {
			$(":input[type=submit]", $(this).parents("form")).removeAttr("clicked");
			$( this ).attr('clicked', true );
		},

		getNotEmptySelectsLength: function() {
			return $('.settings-field-map-table:visible').find( 'tbody tr:visible' ).filter( function () {
				return $( 'select', this ).val() !== '';
			} ).length;
		},

		/**
		 * Make sure there are mapped fields before allowing submission
		 * @param e
		 * @returns {boolean}
		 */
		submitSettings: function( e ) {

			var field_map = $('.settings-field-map-table:visible');

			if( field_map.length > 0 ) {

				// There are only empty selects
				if ( GVImport.getNotEmptySelectsLength() === 0 ) {

					// They were submitting the form, not just saving the configuration
					// [clicked] was added by GVImport.addClickedAttr
					if ( $( e.target ).find( 'input[clicked]' ).hasClass( 'button-primary' ) ) {

						alert( gv_importer_strings.field_mapping_empty );

						// Highlight the field
						$( '#gaddon-setting-row-import_field_map' ).find('tr:first-child select' ).focus();

						return false;
					}
				}
			}

			return true;
		},

		completeImport: function( e ) {
			e.preventDefault();

			var data = {
				action: 'gv_import_complete',
				feed_id: gv_importer_strings.feed_id,
				nonce: gv_importer_strings.nonce
			};

			$( e.target ).html( gv_importer_strings.wrapping_up );

			$.post( ajaxurl, data, function ( response ) {

				$( e.target ).text( gv_importer_strings.complete );

				var url = $( e.target ).attr('href' );

				if( !url || url === '#' ) {
					// Reload the page
					window.location.reload();
				} else {
					// Redirect to the link
					window.location.replace( url );
				}
			} );

		},

		setVars: function() {
			GVImport.selectFields = $( '#gform-settings' ).find('select.gaddon-select:first' ).clone();
			GVImport.resetButton = '#gform-settings button[type=reset]';
			GVImport.smartButton = '#gform-settings button.smart-map';
		},

		/**
		 * Convert the form into the correct type to upload files
		 */
		updateFormParameters: function() {

			// Improve form design
			$( '.settings-field-map-table' )
				.find('thead tr th:last-child').html( gv_importer_strings.column_header ) // Make the label better
					.end()
				.find('tbody tr:odd')
					.addClass('alt'); // Zebra stripe

			// Allow uploads
			$('#gform-settings').attr('enctype', 'multipart/form-data' );
		},

		toggleConsole: function( e ) {
			e.preventDefault();

			var $button = $( this );

			$('.gravityview-importer-console' ).slideToggle( function() {

				var button_text = $( this ).is(':visible') ? gv_importer_strings.hide_console : gv_importer_strings.show_console;

				$button.text( button_text );
			});
		},

		importerComplete: function() {

			$('.gravityview-importer-status.processing' ).fadeOut('fast', function() {
				$('.gravityview-importer-status.complete' ).fadeIn('fast');
			});


			$('.gravityview-importer-console' ).animate({
				height: 200
			}, 1000, function() {
				$( '.gravityview-importer-report' ).slideDown();
			});
		},

		/**
		 * Modify the GVImport.selectFields input to disable existing search fields, then replace the fields with the generated input.
		 * @return {void}
		 */
		updateAvailableFields: function( e ) {

			// Clear out the disabled options first
			$( 'option', GVImport.selectFields ).attr('disabled', null );

			if( GVImport.getNotEmptySelectsLength() === 0 ) {
				$( GVImport.resetButton ).hide();
			} else {
				$( GVImport.resetButton ).fadeIn('fast');
			}

			$('#gform-settings' )

				.find('tr select.gaddon-select' )

				// Update the selectFields var to disable all existing values
				.each( function() {

					if( $( this ).val() !== '' ) {
						GVImport.selectFields
							.find('option[value="'+ $( this ).val() +'"]')
							.attr( 'title', gv_importer_strings.already_mapped )
							.attr('disabled', true);
					}
				})

				.not( e.target )

				// Then once we have the select input finalized, run through again
				// and replace the select inputs with the new one
				.each( function() {

					var select = GVImport.selectFields.clone();

					// Set the value
					select.val( $(this).val() );

					// Enable the option with the current value
					select.find('option:selected').attr('disabled', null );

					// Set the ID and name to what they should be
					select.attr('id', $( this ).attr('id') );
					select.attr('name', $( this ).attr('name') );

					// Replace the select with the generated one
					$( this ).replaceWith( select );
				});


		},
	};

	$( document )
		.on( 'ready', GVImport.init )
		.on('importer-complete', GVImport.importerComplete )
		.on('importer-added', GVImport.importerAdded );

})( jQuery );
